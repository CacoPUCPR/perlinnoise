package br.pucpr.cg;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.IOException;

import br.pucpr.mage.*;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import br.pucpr.mage.phong.DirectionalLight;
import br.pucpr.mage.phong.Material;

public class Terrain implements Scene {

    private float scale = 1;
    private Keyboard keys = Keyboard.getInstance();

    private NoiseGeneration noise = new NoiseGeneration();
    //Dados da cena
    private Camera camera = new Camera();

    private boolean wireframe = false;

    private DirectionalLight light = new DirectionalLight(
            new Vector3f( 1.0f, -3.0f, -1.0f), //direction
            new Vector3f( 0.02f,  0.02f,  0.02f),   //ambient
            new Vector3f( 1.0f,  1.0f,  1.0f),   //diffuse
            new Vector3f( 1.0f,  1.0f,  1.0f));  //specular

    //Dados da malha
    private Mesh mesh;
    private Material material = new Material(
            new Vector3f(1.0f, 1.0f, 1.0f), //ambient
            new Vector3f(0.7f, 0.7f, 0.7f), //diffuse
            new Vector3f(1.0f, 1.0f, 1.0f), //specular
            512.0f);                         //specular power
    private float angleX = 0.0f;
    private float angleY = 0.5f;

    @Override
    public void init() {
        noise.generateNoise();

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        try {
            mesh = MeshFactory.loadTerrain(new File(noise.pathzaoDaMassa), 0.5f,1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        material.setTexture("uTexture", new Texture("grass.jpg"));

        camera.getPosition().y = 200.0f;
        camera.getPosition().z = 200.0f;
    }

    @Override
    public void update(float secs) {
        if (keys.isPressed(GLFW_KEY_ESCAPE))
        {
            glfwSetWindowShouldClose(glfwGetCurrentContext(), GLFW_TRUE);
            return;
        }
        if (keys.isDown(GLFW_KEY_A))
        {
            angleY += secs;
            camera.rotate(0, (float)Math.toRadians(60)*secs);
        }
        if (keys.isDown(GLFW_KEY_D))
        {
            angleY -= secs;
            camera.rotate(0, (float)Math.toRadians(-60)*secs);
        }
        if (keys.isDown(GLFW_KEY_W))
        {
            if(angleX< 0)
            {
                angleX += secs;
                camera.rotate((float)Math.toRadians(60)*secs, 0);
            }
        }
        if (keys.isDown(GLFW_KEY_S))
        {
            if(angleX > -1)
            {
                angleX -= secs;
                camera.rotate((float)Math.toRadians(-60)*secs, 0);
            }
        }

        if (keys.isDown(GLFW_KEY_SPACE))
        {
            angleX = 0;
            angleY = 0;
        }

        if (keys.isDown(GLFW_KEY_UP))
        {
            //aqui se passa a distancia a qual se quer mover mais se quer ir pra frente/tras ou lateral
            //deixei tudo em uma função só para ficar de mais facil manutenção - Caco

            camera.move(60 * secs* 3, Camera.moveTo.forward);
        }
        if (keys.isDown(GLFW_KEY_DOWN))
        {
            camera.move(-60 * secs* 3, Camera.moveTo.forward);
        }
        if (keys.isDown(GLFW_KEY_LEFT))
        {
            camera.move(-60 * secs* 3, Camera.moveTo.lateral);
        }
        if (keys.isDown(GLFW_KEY_RIGHT))
        {
            camera.move(60 * secs* 3, Camera.moveTo.lateral);
        }

        if(keys.isPressed((GLFW_KEY_Q)))
        {
            if(wireframe)
            {
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                wireframe = false;
            }
            else
            {
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                wireframe = true;
            }
        }

        if(keys.isDown(GLFW_KEY_O))
        {
            if(scale< 2f)
                scale += 0.1f;
        }
        if(keys.isDown((GLFW_KEY_I)))
        {
            if(scale>-0.1)
                scale -= 0.1f;
        }
    }

@Override
public void draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Shader shader = mesh.getShader();
    shader.bind()
            .setUniform("uProjection", camera.getProjectionMatrix())
            .setUniform("uView", camera.getViewMatrix())
            .setUniform("uCameraPosition", camera.getPosition());
    light.apply(shader);
    material.apply(shader);
    shader.unbind();

    mesh.setUniform("uWorld", new Matrix4f());
    mesh.setUniform("uScale", scale);
    mesh.draw();
}

    @Override
    public void deinit() {
    }

    public static void main(String[] args) {
        new Window(new Terrain(), "Terrain with lights", 1024, 768).show();
    }
}
